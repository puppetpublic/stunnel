# modules/stunnel
#
# The basic stunnel class.  This installs stunnel and its filter-syslog
# rules, but doesn't set up an stunnel configuration.  To use stunnel to
# contact the MySQL servers, use stunnel::mysql.
#
# $stunnel_enabled: set to true to have the stunnel service start
#   automatically, false otherwise.
#   DEFAULT: false (for wheezy) , true otherwise.
#
# $use_own_default_file: set to true if you do NOT want this class to manage
# the /etc/default/stunnel4
#   DEFAULT: false
#

class stunnel(
  $stunnel_enabled      = undef,
  $use_own_default_file = false,
){

  if ($::lsbdistcodename == 'wheezy') {
    # wheezy uses daemontools, but jessie and above uses systemd.
    include base::daemontools
  }

  # Set the ENABLED value based on the operating system (unless explicitly
  # defined).
  if ($stunnel_enabled == undef) {
    if ($::lsbdistcodename == 'wheezy') {
      $stunnel_enabled_real = false
    } else {
      $stunnel_enabled_real = true
    }
  } else {
    $stunnel_enabled_real = $stunnel_enable
  }


  case $::operatingsystem {
    'debian', 'ubuntu': {
      package { 'stunnel4': ensure => installed }
    }
    'redhat': {
      package { 'stunnel':  ensure => installed }
    }
    default: {
      crit "cannot handle operating system ${::operatingsystem}"
    }
  }

  file {
    '/etc/stunnel':
      ensure => directory;
    '/etc/filter-syslog/stunnel':
      source => 'puppet:///modules/stunnel/etc/filter-syslog/stunnel';
    '/var/log/stunnel':
      ensure => directory;
  }

  # Unless the $use_own_default_file is set to true, install our own
  # version of /etc/default/stunnel4.
  if (!$use_own_default_file) {
    file {
      '/etc/default/stunnel4':
        ensure  => present,
        content => template('stunnel/etc/default/stunnel4.erb'),
    }
  }

}
