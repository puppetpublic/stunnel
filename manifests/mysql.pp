#
# Sets up stunnel to tunnel to the mysql servers.

## Sets up a consistent mysql stunnel to the main servers.
class stunnel::mysql inherits stunnel {
    file { "/etc/stunnel/mysql.conf": 
        source => "puppet:///modules/stunnel/etc/stunnel/mysql.conf";
    }
    case $operatingsystem {
        'redhat': {
            base::daemontools::supervise { "stunnel-mysql":
                source => "puppet:///modules/stunnel/service/stunnel-mysql.run.redhat"
            }
        }
        default: {
            base::daemontools::supervise { "stunnel-mysql":
                source => "puppet:///modules/stunnel/service/stunnel-mysql.run"
            }
        }
    }
}
